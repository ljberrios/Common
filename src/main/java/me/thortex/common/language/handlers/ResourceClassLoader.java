package me.thortex.common.language.handlers;

import me.thortex.common.language.I18n;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class loader for all the {@link I18n} resources.
 *
 * @author Thortex
 */
public class ResourceClassLoader extends ClassLoader {
    private final File dir;

    public ResourceClassLoader(ClassLoader classLoader, JavaPlugin plugin) {
        super(classLoader);
        dir = plugin.getDataFolder();
    }

    @Override
    public URL getResource(String fileName) {
        File file = new File(dir, fileName);
        if (file.exists()) {
            try {
                return file.toURI().toURL();
            } catch (MalformedURLException ignored) {
            }
        }
        return null;
    }

    @Override
    public InputStream getResourceAsStream(String fileName) {
        File file = new File(dir, fileName);
        if (file.exists()) {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException ignored) {
            }
        }
        return null;
    }
}