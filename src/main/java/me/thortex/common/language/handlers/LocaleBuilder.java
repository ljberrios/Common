package me.thortex.common.language.handlers;

import java.util.Locale;

/**
 * Handles {@link Locale} building.
 *
 * @author Thortex
 */
public class LocaleBuilder {
    /**
     * Create a locale from a string.
     *
     * @param str the string
     * @return the locale
     */
    public Locale buildLocale(String str) {
        String[] parts = str.split("_");
        Locale current = null;
        switch (parts.length) {
            case 1:
                current = new Locale(parts[0]);
                break;
            case 2:
                current = new Locale(parts[0], parts[1]);
                break;
            case 3:
                current = new Locale(parts[0], parts[1], parts[2]);
                break;
        }
        return current;
    }
}
