package me.thortex.common.language.handlers;

import me.thortex.common.language.I18n;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;

/**
 * Handles message formats and translation.
 *
 * @author Thortex
 */
public final class TranslationFormatter {
    private final I18n i18n;
    private final Map<String, MessageFormat> messageFormatCache;

    public TranslationFormatter(I18n i18n) {
        this.i18n = i18n;
        messageFormatCache = new HashMap<>();
    }

    /**
     * Translate a message registered in the resource bundles.
     * <p>
     * Basic usage:
     * <pre>
     *   Player target;
     *   Player sender;
     *   if (target == null) {
     *     sender.sendMessage(translate("playerOffline", target));
     *     return;
     *   }
     * </pre>
     * <p>
     * <b>NOTE:</b> The message has to be registered in the resource bundles.
     *
     * @param msg  the message
     * @param args the formatting arguments
     * @return the translation
     */
    public String translate(ResourceBundle bundle, String msg, Object... args) {
        if (args.length == 0) {
            return I18n.NO_DOUBLE_MARK.matcher(translate(bundle, msg)).replaceAll("'");
        } else {
            return format(bundle, msg, args);
        }
    }

    /**
     * Format a message with objects.
     *
     * @param msg  the message
     * @param args the objects
     * @return the formatted string
     */
    public String format(ResourceBundle bundle, String msg, Object... args) {
        String format = translate(bundle, msg);
        MessageFormat messageFormat = messageFormatCache.get(format);
        if (messageFormat == null) {
            try {
                messageFormat = new MessageFormat(format);
            } catch (IllegalArgumentException e) {
                i18n.getPlugin().getLogger().log(Level.SEVERE,
                    "Invalid Translation key for '" + msg + "': " + e.getMessage());

                format = format.replaceAll("\\{(\\D*?)\\}", "\\[$1\\]");
                messageFormat = new MessageFormat(format);
            }
            // Cache the message format
            messageFormatCache.put(format, messageFormat);
        }
        return messageFormat.format(args);
    }

    private String translate(ResourceBundle bundle, String msg) {
        try {
            return bundle.getString(msg);
        } catch (MissingResourceException e) {
            i18n.getPlugin().getLogger().log(Level.WARNING, String
                .format("Missing translation key \"%s\" in translation file %s",
                    e.getKey(), bundle.getLocale().toString()));
            return i18n.getDefaultBundle().getString(msg);
        }
    }
}
