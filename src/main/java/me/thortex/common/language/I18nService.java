package me.thortex.common.language;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import me.thortex.common.service.Service;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.WeakHashMap;

/**
 * Internationalization service class.
 * Caches the players' language settings on join.
 *
 * @author Thortex
 */
@Singleton
@Data
public class I18nService implements Service, Listener {
    private final JavaPlugin plugin;
    private final I18n i18n;

    private final Map<Player, I18nPack> playerLanguageCache;

    @Inject
    public I18nService(I18n i18n) {
        this.i18n = i18n;
        // Use the already injected JavaPlugin
        plugin = i18n.getPlugin();

        playerLanguageCache = new WeakHashMap<>();
    }

    @Override
    public void start() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void stop() {
        HandlerList.unregisterAll(this);
    }

    /**
     * Get a player's current resource bundle depending
     * on his/her cached language settings.
     *
     * @param player the player
     * @return the resource bundle
     */
    public ResourceBundle getBundle(Player player) {
        return playerLanguageCache.get(player).getBundle();
    }

    /**
     * Get a player's current locale depending on
     * his/her cached language settings.
     *
     * @param player the player
     * @return the locale
     */
    public Locale getLocale(Player player) {
        return playerLanguageCache.get(player).getLocale();
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        // Build locale from string
        Locale locale = i18n.getLocaleBuilder().buildLocale("en");
        // Get the specific locale's resource bundle if it exists
        ResourceBundle bundle =
            i18n.getResourceBundle(I18n.MESSAGES + "_" + locale.getLanguage() + ".properties");

        playerLanguageCache.put(player, new I18nPack(locale, bundle));
        player.sendMessage(i18n.translate(bundle, "languageSet"));
    }
}
