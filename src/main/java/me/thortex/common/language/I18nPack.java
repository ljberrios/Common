package me.thortex.common.language;

import lombok.Data;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Represents a player's language settings.
 *
 * @author Thortex
 */
@Data
public class I18nPack {
    /**
     * The locale provided in the player's settings.
     */
    private final Locale locale;

    /**
     * The selected resource bundle for the specified language settings.
     */
    private final ResourceBundle bundle;
}
