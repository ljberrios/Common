package me.thortex.common.language;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import me.thortex.common.language.handlers.LocaleBuilder;
import me.thortex.common.language.handlers.TranslationFormatter;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

/**
 * Internationalization management class.
 *
 * @author Thortex
 */
@Singleton
@Data
public final class I18n {
    /**
     * The name of the messages's resource file.
     */
    public static final String MESSAGES = "messages";

    /**
     * Pattern to avoid double marks in messages.
     */
    public static final Pattern NO_DOUBLE_MARK = Pattern.compile("''");

    private final JavaPlugin plugin;

    private final LocaleBuilder localeBuilder;
    private final TranslationFormatter translationFormatter;

    private final Locale defaultLocale;
    private final ResourceBundle defaultBundle;

    @Inject
    public I18n(JavaPlugin plugin) {
        this.plugin = plugin;

        localeBuilder = new LocaleBuilder();
        translationFormatter = new TranslationFormatter(this);

        defaultLocale = Locale.getDefault();
        defaultBundle = getResourceBundle(MESSAGES + ".properties");
    }

    /**
     * Translate a message registered in the resource bundles.
     * <p>
     * Basic usage:
     * <pre>
     *   Player target;
     *   Player sender;
     *   if (target == null) {
     *     sender.sendMessage(translate("playerOffline", target));
     *     return;
     *   }
     * </pre>
     * <p>
     * <b>NOTE:</b> The message HAS to be registered in the resource bundles.
     *
     * @param msg  the message
     * @param args the formatting arguments
     * @return the translation
     */
    public String translate(ResourceBundle bundle, String msg, Object... args) {
        return translationFormatter.translate(bundle, msg, args);
    }

    /**
     * Get a resource bundle from a given file name.
     * The folder being scanned for this is the resources folder.
     *
     * @param fileName the file's name with the format. (file.yml)
     * @return the resource bundle
     */
    public ResourceBundle getResourceBundle(String fileName) {
        try {
            return new PropertyResourceBundle(plugin.getResource(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return defaultBundle;
    }
}