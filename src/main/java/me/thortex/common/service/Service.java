package me.thortex.common.service;

/**
 * Represents a class that can be started and stopped at any given time.
 *
 * @author Thortex
 */
public interface Service {
    /**
     * Start the service.
     */
    void start();

    /**
     * Stop the service.
     */
    void stop();
}
