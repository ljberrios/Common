package me.thortex.common.projectile;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import me.thortex.common.projectile.events.CustomProjectileExpireEvent;
import me.thortex.common.projectile.events.CustomProjectileHitBlockEvent;
import me.thortex.common.projectile.events.CustomProjectileHitEntityEvent;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * Represents a custom projectile.
 *
 * @author Thortex
 */
@Data
@Builder
@AllArgsConstructor
public class Projectile {
    private org.bukkit.entity.Entity thrownEntity;
    private LivingEntity thrower;

    private double damage;
    private long expireTime;
    private boolean blockCollidable;
    private boolean playerCollidable;
    private boolean mobCollidable;
    private boolean pickable;

    /**
     * @return if it has collided with anything
     */
    public boolean collided() {
        if ((this.expireTime != -1L) && (System.currentTimeMillis() > this.expireTime)) {
            Bukkit.getServer().getPluginManager().callEvent(new CustomProjectileExpireEvent(this));
            return true;
        }

        double distanceToEntity = 0.0D;
        LivingEntity victim = null;

        Entity nmsEntity = ((CraftEntity) thrownEntity).getHandle();
        Vec3D vec3d = new Vec3D(nmsEntity.locX, nmsEntity.locY, nmsEntity.locZ);
        Vec3D vec3d1 = new Vec3D(nmsEntity.locX + nmsEntity.motX, nmsEntity.locY + nmsEntity.motY,
            nmsEntity.locZ + nmsEntity.motZ);

        MovingObjectPosition finalObjectPosition = nmsEntity.world.rayTrace(vec3d, vec3d1, false, true, false);
        vec3d = new Vec3D(nmsEntity.locX, nmsEntity.locY, nmsEntity.locZ);
        vec3d1 = new Vec3D(nmsEntity.locX + nmsEntity.motX, nmsEntity.locY + nmsEntity.motY,
            nmsEntity.locZ + nmsEntity.motZ);

        if (finalObjectPosition != null) {
            vec3d1 = new Vec3D(finalObjectPosition.pos.a, finalObjectPosition.pos.b,
                finalObjectPosition.pos.c);
        }

        for (Object entity : ((CraftWorld) thrownEntity.getWorld()).getHandle()
            .getEntities(((CraftEntity) thrownEntity).getHandle(),
                ((CraftEntity) thrownEntity).getHandle().getBoundingBox()
                    .a(((CraftEntity) thrownEntity).getHandle().motX,
                        ((CraftEntity) thrownEntity).getHandle().motY,
                        ((CraftEntity) thrownEntity).getHandle().motZ)
                    .grow(1.0D, 1.0D, 1.0D))) {
            if ((entity instanceof Entity)) {
                org.bukkit.entity.Entity bukkitEntity =
                    ((Entity) entity).getBukkitEntity();

                if ((bukkitEntity instanceof LivingEntity)) {
                    LivingEntity ent = (LivingEntity) bukkitEntity;
                    if (!ent.equals(thrower)) {
                        if ((ent instanceof Player) &&
                            (((Player) ent).getGameMode() == GameMode.CREATIVE)) {
                            return true;
                        }

                        float f1 = (float) (nmsEntity.getBoundingBox().a() * 0.6D);
                        AxisAlignedBB axisalignedbb1 =
                            ((CraftEntity) ent).getHandle().getBoundingBox()
                                .grow(f1, f1, f1);
                        MovingObjectPosition entityCollisionPosition =
                            axisalignedbb1.a(vec3d, vec3d1);

                        if (entityCollisionPosition != null) {
                            double d1 = vec3d.distanceSquared(entityCollisionPosition.pos);
                            if ((d1 < distanceToEntity) || (distanceToEntity == 0.0D)) {
                                victim = ent;
                                distanceToEntity = d1;
                            }
                        }
                    }
                }
            }
        }

        if (victim != null) {
            EntityLiving craftVictim = ((CraftLivingEntity) victim).getHandle();
            finalObjectPosition = new MovingObjectPosition(craftVictim);
            if (craftVictim instanceof EntityPlayer && playerCollidable) {
                Bukkit.getServer().getPluginManager()
                    .callEvent(new CustomProjectileHitEntityEvent(this, victim));
                return true;

            } else if (!(craftVictim instanceof EntityPlayer) && mobCollidable) {
                Bukkit.getServer().getPluginManager()
                    .callEvent(new CustomProjectileHitEntityEvent(this, victim));
                return true;
            }
        }

        if (finalObjectPosition != null) {
            if (blockCollidable) {
                Block block = thrownEntity.getWorld()
                    .getBlockAt(finalObjectPosition.a().getX(), finalObjectPosition.a().getY(),
                        finalObjectPosition.a().getZ());
                if (block.getType() != org.bukkit.Material.AIR && !block.isLiquid()) {
                    nmsEntity.motX = ((float) (finalObjectPosition.pos.a - nmsEntity.locX));
                    nmsEntity.motY = ((float) (finalObjectPosition.pos.b - nmsEntity.locY));
                    nmsEntity.motZ = ((float) (finalObjectPosition.pos.c - nmsEntity.locZ));
                    float f2 = MathHelper.sqrt(nmsEntity.motX * nmsEntity.motX +
                        nmsEntity.motY * nmsEntity.motY +
                        nmsEntity.motZ * nmsEntity.motZ);
                    nmsEntity.locX -= nmsEntity.motX / f2 * 0.0500000007450581D;
                    nmsEntity.locY -= nmsEntity.motY / f2 * 0.0500000007450581D;
                    nmsEntity.locZ -= nmsEntity.motZ / f2 * 0.0500000007450581D;
                    Bukkit.getServer().getPluginManager()
                        .callEvent(new CustomProjectileHitBlockEvent(this, block));
                    return true;
                }
            }
        }
        return false;
    }
}
