package me.thortex.common.projectile;

import com.google.common.collect.MapMaker;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import me.thortex.common.service.Service;
import me.thortex.common.update.UpdateEvent;
import me.thortex.common.update.UpdateType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * Handles custom projectiles.
 *
 * @author Thortex
 */
@Singleton
@Data
public class ProjectileService implements Service, Listener {
    private final JavaPlugin plugin;
    private final ConcurrentMap<Entity, Projectile> projectiles;

    @Inject
    public ProjectileService(JavaPlugin plugin) {
        this.plugin = plugin;
        projectiles = new MapMaker()
            .concurrencyLevel(4)
            .weakKeys()
            .makeMap();
    }

    @Override
    public void start() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void stop() {
        HandlerList.unregisterAll(this);
    }

    /**
     * Add a projectile.
     *
     * @param projectile the projectile
     */
    public void addProjectile(Projectile projectile) {
        projectiles.putIfAbsent(projectile.getThrownEntity(), projectile);
    }

    /**
     * Get a projectile from a thrown entity.
     *
     * @param thrown the entity
     * @return the projectile
     */
    public Projectile getProjectile(Entity thrown) {
        return projectiles.get(thrown);
    }

    @EventHandler
    public void onUpdate(UpdateEvent event) {
        if (event.getType() != UpdateType.TICK) return;
        for (Iterator<Map.Entry<Entity, Projectile>> itr = projectiles.entrySet().iterator(); itr.hasNext(); ) {
            Map.Entry<Entity, Projectile> entry = itr.next();
            Projectile projectile = entry.getValue();
            if (projectile == null || !isEntityValid(entry.getKey())) {
                itr.remove();
                continue;
            }

            projectile.collided();
        }
    }

    @EventHandler
    public void onPlayerPickup(PlayerPickupItemEvent event) {
        preventPickup(event.getItem(), event);
    }

    @EventHandler
    public void onInventoryPickup(InventoryPickupItemEvent event) {
        preventPickup(event.getItem(), event);
    }

    private void preventPickup(Item item, Cancellable event) {
        projectiles.entrySet().iterator().forEachRemaining(entry -> {
            Entity entity = entry.getKey();
            if (isEntityValid(entity) && entity.equals(item)) {
                event.setCancelled(true);
            } else {
                projectiles.remove(entity);
            }
        });
    }

    private boolean isEntityValid(Entity entity) {
        return entity != null && !entity.isDead() && entity.isValid();
    }
}
