package me.thortex.common.projectile.events;

import lombok.Data;
import me.thortex.common.projectile.Projectile;
import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Data

public class CustomProjectileHitBlockEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    private Projectile projectile;
    private Block collidedBlock;

    public CustomProjectileHitBlockEvent(Projectile projectile, Block collidedBlock) {
        this.projectile = projectile;
        this.collidedBlock = collidedBlock;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
