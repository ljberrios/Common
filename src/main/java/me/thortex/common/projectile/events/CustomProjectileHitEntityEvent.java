package me.thortex.common.projectile.events;

import lombok.Data;
import me.thortex.common.projectile.Projectile;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Data

public class CustomProjectileHitEntityEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    private Projectile projectile;
    private LivingEntity collidedEntity;

    public CustomProjectileHitEntityEvent(Projectile projectile, LivingEntity collidedEntity) {
        this.projectile = projectile;
        this.collidedEntity = collidedEntity;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}