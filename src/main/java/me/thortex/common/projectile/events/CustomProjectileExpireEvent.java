package me.thortex.common.projectile.events;

import lombok.Data;
import me.thortex.common.projectile.Projectile;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Data

public class CustomProjectileExpireEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;

    private Projectile projectile;

    public CustomProjectileExpireEvent(Projectile projectile) {
        this.projectile = projectile;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}