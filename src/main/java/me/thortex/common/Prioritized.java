package me.thortex.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Apply this to services or commands that should be initialized before other "normal" commands and services.
 *
 * @author Thortex
 */
@Target({ElementType.TYPE})
@Retention(RUNTIME)
public @interface Prioritized {}
