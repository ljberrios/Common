package me.thortex.common.scoreboard.utility;

import org.bukkit.ChatColor;

/**
 * String utilities for scoreboards.
 *
 * @author Thortex
 */
public final class Strings {
    private Strings() {}

    /**
     * Translate color codes for the given string.
     *
     * @param string the string to translate
     * @return the translated string
     */
    public static String translateColors(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    /**
     * Repeat a string the given amount of times.
     *
     * @param string the string to repeat
     * @param count  the amount of times
     * @return the new string
     */
    public static String repeat(String string, int count) {
        if (count <= 1) {
            return count == 0 ? "" : string;
        } else {
            int len = string.length();
            long longSize = (long) len * (long) count;
            int size = (int) longSize;
            if ((long) size != longSize) {
                throw new ArrayIndexOutOfBoundsException("Required array size too large: " + longSize);
            } else {
                char[] array = new char[size];
                string.getChars(0, len, array, 0);
                int n;
                for (n = len; n < size - n; n <<= 1) {
                    System.arraycopy(array, 0, array, n, n);
                }
                System.arraycopy(array, 0, array, n, size - n);
                return new String(array);
            }
        }
    }
}