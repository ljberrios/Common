package me.thortex.common.scoreboard.utility;

import lombok.Data;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.Map;
import java.util.UUID;

/**
 * Represents a fake {@link OfflinePlayer}.
 *
 * @author Thortex
 */
@Data
public class FakePlayer implements OfflinePlayer {
    private final String name;

    private Team team;
    private int offset;

    public FakePlayer(String name, Team team, int offset) {
        this.name = name;
        this.team = team;
        this.offset = offset;
    }

    /**
     * Get the fake player's full name with team prefix and suffix.
     *
     * @return the name
     */
    public String getFullName() {
        if (team == null) {
            return name;
        } else if (team.getSuffix() == null) {
            return team.getPrefix() + name;
        } else {
            return team.getPrefix() + name + team.getSuffix();
        }
    }

    @Override
    public boolean isOnline() {
        return true;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public UUID getUniqueId() {
        return UUID.randomUUID();
    }

    @Override
    public boolean isBanned() {
        return false;
    }

    @Override
    public void setBanned(boolean banned) {}

    @Override
    public boolean isWhitelisted() {
        return false;
    }

    @Override
    public void setWhitelisted(boolean whitelisted) {}

    @Override
    public Player getPlayer() {
        return null;
    }

    @Override
    public long getFirstPlayed() {
        return 0;
    }

    @Override
    public long getLastPlayed() {
        return 0;
    }

    @Override
    public boolean hasPlayedBefore() {
        return false;
    }

    @Override
    public Location getBedSpawnLocation() {
        return null;
    }

    @Override
    public Map<String, Object> serialize() {
        return null;
    }

    @Override
    public boolean isOp() {
        return false;
    }

    @Override
    public void setOp(boolean op) {}
}
