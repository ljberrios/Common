package me.thortex.common.scoreboard.utility;

import me.thortex.common.scoreboard.ScoreboardEntry;

import java.util.LinkedList;
import java.util.List;

/**
 * An utility to make pretty entries for the scoreboards, without calculating the positions by yourself.
 *
 * @author Thortex
 */
public final class EntryBuilder {
    private final LinkedList<ScoreboardEntry> entries;

    public EntryBuilder() {
        entries = new LinkedList<>();
    }

    /**
     * Append a blank line.
     *
     * @return this
     */
    public EntryBuilder blank() {
        return next("");
    }

    /**
     * Append a new line with specified text.
     *
     * @param string the text
     * @return this
     */
    public EntryBuilder next(String string) {
        entries.add(new ScoreboardEntry(adapt(string), entries.size()));
        return this;
    }

    /**
     * Returns a map of entries.
     *
     * @return the map
     */
    public List<ScoreboardEntry> build() {
        entries.forEach(entry -> entry.setPosition(entries.size() - entry.getPosition()));
        return entries;
    }

    private String adapt(String entry) {
        // Cut off the exceeded part if needed
        if (entry.length() > 48) {
            entry = entry.substring(0, 47);
        }
        return Strings.translateColors(entry);
    }
}