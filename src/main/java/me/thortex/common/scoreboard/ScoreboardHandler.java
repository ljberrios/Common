package me.thortex.common.scoreboard;

import org.bukkit.entity.Player;

import java.util.List;

/**
 * Represents the handler to determine the title and entries of a scoreboard.
 *
 * @author Thortex
 */
public interface ScoreboardHandler {
    /**
     * Determines the title to display for this player. If null returned, title
     * automatically becomes a blank line.
     *
     * @param player the player
     * @return the title
     */
    String getTitle(Player player);

    /**
     * Determines the entries to display for this player. If null returned, the
     * entries are not updated.
     *
     * @param player the player
     * @return the entries
     */
    List<ScoreboardEntry> getEntries(Player player);
}