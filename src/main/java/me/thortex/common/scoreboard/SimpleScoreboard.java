package me.thortex.common.scoreboard;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import lombok.Data;
import me.thortex.common.scoreboard.utility.FakePlayer;
import me.thortex.common.scoreboard.utility.Strings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents a normal scoreboard.
 *
 * @author Thortex
 */
@Data
public class SimpleScoreboard {
    private static final String TEAM_PREFIX = "Scoreboard_";
    private static int TEAM_COUNTER = 0;

    private final JavaPlugin plugin;

    private final Scoreboard scoreboard;
    private final Objective objective;

    private Player holder;
    private long updateInterval;

    private boolean activated;
    private ScoreboardHandler handler;
    private BukkitRunnable updateTask;

    private final Map<FakePlayer, Integer> entryCache;
    private final Table<String, Integer, FakePlayer> playerCache;
    private final Table<Team, String, String> teamCache;

    public SimpleScoreboard(JavaPlugin plugin, Player holder, long updateInterval, ScoreboardHandler handler) {
        this.plugin = plugin;
        this.holder = holder;
        this.updateInterval = updateInterval;
        this.handler = handler;

        entryCache = new ConcurrentHashMap<>();
        playerCache = HashBasedTable.create();
        teamCache = HashBasedTable.create();

        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        scoreboard.registerNewObjective("board", "dummy").setDisplaySlot(DisplaySlot.SIDEBAR);
        objective = scoreboard.getObjective(DisplaySlot.SIDEBAR);
    }

    /**
     * Activate the scoreboard.
     */
    public void activate() {
        if (activated) return;
        else if (handler == null) {
            throw new IllegalArgumentException("Scoreboard handler not set");
        }
        activated = true;
        // Set to the custom scoreboard
        holder.setScoreboard(scoreboard);
        // And start updating on a desired interval
        updateTask = new BukkitRunnable() {
            @Override
            public void run() {
                update();
            }
        };
        updateTask.runTaskTimer(plugin, 0, updateInterval);
    }

    /**
     * Deactivate the scoreboard.
     */
    public void deactivate() {
        if (!activated) return;
        activated = false;
        // If the player's on and has this scoreboard, set to main scoreboard
        if (holder != null && holder.getScoreboard().equals(scoreboard)) {
            synchronized (this) {
                holder.setScoreboard((Bukkit.getScoreboardManager().getMainScoreboard()));
            }
        }
        // Unregister teams that are created for this scoreboard
        teamCache.rowKeySet().forEach(Team::unregister);
        // Stop updating
        updateTask.cancel();
    }

    /**
     * Update the scoreboard.
     */
    @SuppressWarnings("deprecation")
    public void update() {
        // If the player is not online or doesn't have the scoreboard anymore, deactivate
        if (holder == null || !holder.getScoreboard().equals(scoreboard)) {
            deactivate();
            return;
        }

        // Title
        String handlerTitle = handler.getTitle(holder);
        String finalTitle = Strings.translateColors(handlerTitle != null ? handlerTitle : ChatColor.BOLD.toString());
        if (!objective.getDisplayName().equals(finalTitle)) {
            objective.setDisplayName(Strings.translateColors(finalTitle));
        }

        // Entries
        List<ScoreboardEntry> passed = handler.getEntries(holder);
        Map<String, Integer> appeared = new HashMap<>();
        Map<FakePlayer, Integer> current = new HashMap<>();
        if (passed == null) return;
        for (ScoreboardEntry entry : passed) {
            // Handle the entry
            String key = entry.getName();
            Integer score = entry.getPosition();
            if (key.length() > 48) {
                key = key.substring(0, 47);
            }
            String appearance;
            if (key.length() > 16) {
                appearance = key.substring(16);
            } else {
                appearance = key;
            }
            if (!appeared.containsKey(appearance)) {
                appeared.put(appearance, -1);
            }
            appeared.put(appearance, appeared.get(appearance) + 1);
            // Get fake player
            FakePlayer faker = getFakePlayer(key, appeared.get(appearance));
            // Set score
            objective.getScore(faker).setScore(score);
            // Update references
            entryCache.put(faker, score);
            current.put(faker, score);
        }
        appeared.clear();
        // Remove duplicated or non-existent entries
        entryCache.keySet().stream()
            .filter(fakePlayer -> !current.containsKey(fakePlayer))
            .forEach(fakePlayer -> {
                entryCache.remove(fakePlayer);
                scoreboard.resetScores(fakePlayer.getName());
            });
    }

    @SuppressWarnings("deprecation")
    private FakePlayer getFakePlayer(String text, int offset) {
        Team team = null;
        String name;
        // If the text has a length less than 16, teams need not to be be created
        if (text.length() <= 16) {
            name = text + Strings.repeat(" ", offset);
        } else {
            String prefix;
            String suffix = "";
            offset++;
            // Otherwise, iterate through the string and cut off prefix and suffix
            prefix = text.substring(0, 16 - offset);
            name = text.substring(16 - offset);
            if (name.length() > 16) {
                name = name.substring(0, 16);
            }
            if (text.length() > 32) {
                suffix = text.substring(32 - offset);
            }
            // If teams already exist, use them
            for (Team other : teamCache.rowKeySet()) {
                if (other.getPrefix().equals(prefix) && other.getSuffix().equals(suffix)) {
                    team = other;
                }
            }
            // Otherwise create them
            if (team == null) {
                team = scoreboard.registerNewTeam(TEAM_PREFIX + TEAM_COUNTER++);
                team.setPrefix(prefix);
                team.setSuffix(suffix);
                teamCache.put(team, prefix, suffix);
            }
        }
        FakePlayer faker;
        if (!playerCache.contains(name, offset)) {
            faker = new FakePlayer(name, team, offset);
            playerCache.put(name, offset, faker);
            if (faker.getTeam() != null) {
                faker.getTeam().addPlayer(faker);
            }
        } else {
            faker = playerCache.get(name, offset);
            if (team != null && faker.getTeam() != null) {
                faker.getTeam().removePlayer(faker);
            }
            faker.setTeam(team);
            if (faker.getTeam() != null) {
                faker.getTeam().addPlayer(faker);
            }
        }
        return faker;
    }
}