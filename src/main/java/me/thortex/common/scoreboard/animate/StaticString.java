package me.thortex.common.scoreboard.animate;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents a normal, static, string.
 *
 * @author Thortex
 */
@Data
@AllArgsConstructor
public class StaticString implements AnimatableString {
    private final String string;

    @Override
    public String current() {
        return string;
    }

    @Override
    public String previous() {
        return string;
    }

    @Override
    public String next() {
        return string;
    }
}