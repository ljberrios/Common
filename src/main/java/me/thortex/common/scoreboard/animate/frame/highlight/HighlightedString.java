package me.thortex.common.scoreboard.animate.frame.highlight;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.thortex.common.scoreboard.animate.frame.FrameAnimatedString;

/**
 * Represents a highlighted string.
 *
 * @author Thortex
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class HighlightedString extends FrameAnimatedString {
    private String context;
    private String normalFormat;
    private String highlightFormat;
    private String prefix;
    private String suffix;

    /**
     * Create a highlighted string.
     *
     * @param context         the context
     * @param normalFormat    the normal format
     * @param highlightFormat the highlight format
     */
    public HighlightedString(String context, String normalFormat,
                             String highlightFormat) {
        this.context = context;
        this.normalFormat = normalFormat;
        this.highlightFormat = highlightFormat;

        prefix = "";
        suffix = "";
        generateFrames();
    }

    /**
     * Create a highlighted scoreboard string.
     *
     * @param context         the context
     * @param normalFormat    the normal format
     * @param highlightFormat the highlight format
     * @param prefix          the prefix
     * @param suffix          the suffix
     */
    public HighlightedString(String context, String normalFormat,
                             String highlightFormat, String prefix,
                             String suffix) {
        this.context = context;
        this.normalFormat = normalFormat;
        this.highlightFormat = highlightFormat;
        this.prefix = prefix;
        this.suffix = suffix;
        generateFrames();
    }

    /**
     * Generate the frames for the highlighting.
     */
    protected void generateFrames() {
        int index = 0;
        while (index < context.length()) {
            // Add highlighted string if the current character isn't a space
            if (context.charAt(index) != ' ') {
                String highlighted = normalFormat
                        + context.substring(0, index) + highlightFormat
                        + context.charAt(index) + normalFormat
                        + context.substring(index + 1, context.length());
                getFrames().add(prefix + highlighted + suffix);
            } else {
                getFrames().add(prefix + normalFormat + context + suffix);
            }
            index++;
        }
    }
}