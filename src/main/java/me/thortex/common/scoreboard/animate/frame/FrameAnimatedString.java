package me.thortex.common.scoreboard.animate.frame;

import lombok.Data;
import me.thortex.common.scoreboard.animate.AnimatableString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represents a {@link org.bukkit.scoreboard.Scoreboard} string that can be animated through frames.
 *
 * @author Thortex
 */
@Data
public class FrameAnimatedString implements AnimatableString {
    private List<String> frames;
    private int currentFrame;

    public FrameAnimatedString() {
        frames = new ArrayList<>();
        currentFrame = -1;
    }

    /**
     * Create it with the given array of frames.
     *
     * @param frames the frames
     */
    public FrameAnimatedString(String... frames) {
        this.frames = Arrays.asList(frames);
    }

    /**
     * Create it with the given list of frames.
     *
     * @param frames the frames
     */
    public FrameAnimatedString(List<String> frames) {
        this.frames = frames;
    }

    @Override
    public String current() {
        if (currentFrame == -1) return null;
        return frames.get(currentFrame);
    }

    @Override
    public String next() {
        currentFrame++;
        if (currentFrame == frames.size()) {
            currentFrame = 0;
        }
        return frames.get(currentFrame);
    }

    @Override
    public String previous() {
        currentFrame--;
        if (currentFrame == -1) {
            currentFrame = frames.size() - 1;
        }
        return frames.get(currentFrame);
    }
}