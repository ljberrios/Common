package me.thortex.common.scoreboard.animate;

/**
 * Represents a string that can be animated.
 *
 * @author Thortex
 */
public interface AnimatableString {
    /**
     * The current part of the string.
     *
     * @return the string
     */
    String current();

    /**
     * The next part of the string.
     *
     * @return the string
     */
    String next();

    /**
     * The previous part of the string.
     *
     * @return the string
     */
    String previous();
}