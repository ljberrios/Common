package me.thortex.common.scoreboard.animate.frame.highlight;

import lombok.Data;
import org.bukkit.ChatColor;

/**
 * Scrollable(Highlighted) text alternate class.
 *
 * @author Thortex
 */
@Data
public final class ColorScroll {
    public enum ScrollType {
        FORWARD,
        BACKWARD;
    }

    private final String context;
    private final String colorBefore;
    private final String colorAfter;
    private final String colorMid;

    private final boolean bold;
    private final boolean upperCaseMid;

    private final ChatColor textColor;

    private ScrollType scrollType;

    private int position;

    public ColorScroll(ChatColor textColor, String context, String colorMid, String colorBefore, String colorAfter,
                       boolean bold, boolean upperCaseMid, ScrollType scrollType) {
        this.context = context;
        this.colorMid = colorMid;
        this.bold = bold;
        this.colorBefore = colorBefore;
        this.colorAfter = colorAfter;
        this.textColor = textColor;
        this.upperCaseMid = upperCaseMid;
        this.scrollType = scrollType;
        this.position = scrollType == ScrollType.FORWARD ? -1 : context.length();
    }

    /**
     * Get the next highlighted string.
     *
     * @return the string
     */
    public String next() {
        if (position >= context.length()) {
            String one = context.substring(position - 1, context.length() - 1);
            if (bold) {
                String two =
                    upperCaseMid ? colorMid + ChatColor.BOLD + one.toUpperCase() : colorMid + ChatColor.BOLD + one;
                String fin = textColor + "" + ChatColor.BOLD + context.substring(0, context.length() - 1) +
                    colorBefore + ChatColor.BOLD +
                    context.substring(context.length() - 1, context.length()) + two;
                if (getScrollType() == ScrollType.FORWARD) {
                    position = -1;
                } else {
                    position--;
                }
                return fin;
            } else {
                String two = upperCaseMid ? colorMid + one.toUpperCase() : colorMid + one;
                String fin = textColor + context.substring(0, context.length() - 1) +
                    colorBefore + context.substring(context.length() - 1, context.length()) + two;
                if (getScrollType() == ScrollType.FORWARD) {
                    position = -1;
                } else {
                    position--;
                }
                return fin;
            }
        }

        if (position <= -1) {
            if (getScrollType() == ScrollType.FORWARD) {
                position++;
            } else {
                position = context.length();
            }
            if (bold) {
                return colorBefore + ChatColor.BOLD + context.substring(0, 1) + textColor + ChatColor.BOLD +
                    context.substring(1, context.length());
            } else {
                return colorBefore + context.substring(0, 1) + textColor + context.substring(1, context.length());
            }
        }

        if (position == 0) {
            String one = context.substring(0, 1);
            if (bold) {
                String two =
                    upperCaseMid ? colorMid + ChatColor.BOLD + one.toUpperCase() : colorMid + ChatColor.BOLD + one;
                String fin = two + colorAfter + ChatColor.BOLD +
                    context.substring(1, 2) + textColor + ChatColor.BOLD +
                    context.substring(2, context.length());
                if (getScrollType() == ScrollType.FORWARD) {
                    position++;
                } else {
                    position--;
                }
                return fin;
            } else {
                String two = upperCaseMid ? colorMid + one.toUpperCase() : colorMid + one;
                String fin = two + colorAfter +
                    context.substring(1, 2) + textColor +
                    context.substring(2, context.length());
                if (getScrollType() == ScrollType.FORWARD) {
                    position++;
                } else {
                    position--;
                }
                return fin;
            }
        }

        if (position >= 1) {
            String one = context.substring(0, position);
            String two = context.substring(position + 1, context.length());
            if (bold) {
                String three =
                    upperCaseMid ? colorMid + ChatColor.BOLD + context.substring(position, position + 1).toUpperCase() :
                        colorMid + ChatColor.BOLD + context.substring(position, position + 1);
                String fin = null;
                int m = one.length();
                int l = two.length();
                String first = m <= 1 ? colorBefore + ChatColor.BOLD + one :
                    ChatColor.BOLD + one.substring(0, one.length() - 1) + colorBefore +
                        ChatColor.BOLD + one.substring(one.length() - 1, one.length());
                String second = l <= 1 ? colorAfter + ChatColor.BOLD + two :
                    colorAfter + ChatColor.BOLD + two.substring(0, 1) +
                        textColor + ChatColor.BOLD + two.substring(1, two.length());
                fin = textColor + first + three + second;
                if (getScrollType() == ScrollType.FORWARD) {
                    position++;
                } else {
                    position--;
                }
                return fin;
            } else {
                String three = upperCaseMid ? colorMid + context.substring(position, position + 1).toUpperCase() :
                    colorMid + context.substring(position, position + 1);
                String fin = null;
                int m = one.length();
                int l = two.length();
                String first = m <= 1 ? colorBefore + one : one.substring(0, one.length() - 1) + colorBefore +
                    one.substring(one.length() - 1, one.length());
                String second = l <= 1 ? colorAfter + two :
                    colorAfter + two.substring(0, 1) +
                        textColor + two.substring(1, two.length());
                fin = textColor + first + three + second;
                if (getScrollType() == ScrollType.FORWARD) {
                    position++;
                } else {
                    position--;
                }
                return fin;
            }
        }
        return "";
    }
}
