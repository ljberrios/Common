package me.thortex.common.scoreboard.animate.frame;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a {@link org.bukkit.scoreboard.Scoreboard} string that can be scrolled through.
 *
 * @author Thortex
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ScrollableString extends FrameAnimatedString {
    private ChatColor resetColor;

    private int position;
    private List<String> scrollEntries;

    /**
     * Create a scrollable string.
     *
     * @param message      the message
     * @param width        the width
     * @param spaceBetween the space between scrolls
     */
    public ScrollableString(String message, int width, int spaceBetween) {
        scrollEntries = new ArrayList<>();
        resetColor = ChatColor.RESET;

        // String is too short for window?
        if (message.length() < width) {
            StringBuilder sb = new StringBuilder(message);
            while (sb.length() < width) {
                sb.append(" ");
            }
            message = sb.toString();
        }

        // Allow for colours which add 2 to the width
        width -= 2;

        // Invalid width/space size
        if (width < 1) {
            width = 1;
        }
        if (spaceBetween < 0) {
            spaceBetween = 0;
        }

        // Add sub-strings
        for (int i = 0; i < message.length() - width; i++) {
            scrollEntries.add(message.substring(i, i + width));
        }

        // Add space between repeats
        StringBuilder space = new StringBuilder();
        for (int i = 0; i < spaceBetween; ++i) {
            scrollEntries.add(message.substring(message.length() - width
                    + (i > width ? width : i), message.length()) + space);
            if (space.length() < width) space.append(" ");
        }
        // Wrap
        for (int i = 0; i < width - spaceBetween; ++i) {
            scrollEntries.add(message.substring(message.length() - width
                + spaceBetween + i, message.length()) + space + message.substring(0, i));
        }
        // Join up
        for (int i = 0; i < spaceBetween; i++) {
            if (i > space.length()) break;
            scrollEntries.add(space.substring(0, space.length() - i)
                    + message.substring(0, width - (spaceBetween > width ? width : spaceBetween) + i));
        }
    }

    @Override
    public String next() {
        StringBuilder builder = getNext();
        if (builder.charAt(builder.length() - 1) == ChatColor.COLOR_CHAR)
            builder.setCharAt(builder.length() - 1, ' ');
        if (builder.charAt(0) == ChatColor.COLOR_CHAR) {
            ChatColor color = ChatColor.getByChar(builder.charAt(1));
            if (color != null) {
                resetColor = color;
                builder = getNext();
                if (builder.charAt(0) != ' ') {
                    builder.setCharAt(0, ' ');
                }
            }
        }
        return resetColor + builder.toString();
    }

    private StringBuilder getNext() {
        return new StringBuilder(scrollEntries.get(position++ % scrollEntries.size()));
    }
}