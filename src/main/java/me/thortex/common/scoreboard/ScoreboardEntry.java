package me.thortex.common.scoreboard;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Represents an entry in the scoreboard.
 *
 * @author Thortex
 */
@Data
@AllArgsConstructor
public class ScoreboardEntry {
    private String name;
    private int position;
}
