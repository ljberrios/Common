package me.thortex.common.config;

import lombok.Data;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;

/**
 * Handles new file configurations.
 *
 * @author Thortex
 */
@Data
public class Configuration {
    protected final JavaPlugin plugin;
    protected File file;

    protected FileConfiguration config;

    public Configuration(JavaPlugin plugin, File file) {
        this(plugin, file, null);
    }

    public Configuration(JavaPlugin plugin, File file, String resource) {
        this.plugin = plugin;
        this.file = file;

        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
                if (resource != null) {
                    // Get the resource from the resources folder
                    plugin.saveResource(resource, true);
                }
            }

            config = YamlConfiguration.loadConfiguration(file);
            config.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            plugin.getLogger().severe("Unable to create config '" + file.getName() + "'");
        }
    }

    /**
     * Save the config.
     */
    public void save() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reload the config.
     */
    public void reload() {
        config = YamlConfiguration.loadConfiguration(file);
        Reader stream = getDefaultReader();
        if (stream != null) {
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(stream);
            config.setDefaults(defConfig);
        }
    }

    /**
     * Add a default value.
     *
     * @param path  the path
     * @param value the value
     */
    public void setDefault(String path, Object value) {
        if (!config.contains(path)) {
            config.set(path, value);
            save();
        }
    }

    private Reader getDefaultReader() {
        try {
            return new InputStreamReader(plugin.getResource(file.getName()), "UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
