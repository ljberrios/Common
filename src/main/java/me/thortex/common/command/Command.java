package me.thortex.common.command;

import org.bukkit.command.CommandSender;

import java.util.List;

/**
 * Represents a command in the game.
 *
 * @author Thortex
 */
public interface Command {
    /**
     * Get the labels that this command can be called from.
     *
     * @return a list of aliases
     */
    List<String> getAliases();

    /**
     * Execute this command.
     *
     * @param sender the one who used this command
     * @param args   the arguments of the command
     */
    void execute(CommandSender sender, String[] args);

    /**
     * If the given player has the permission to use this command.
     *
     * @param sender the one who used this command
     * @return if he/she has permission to use it
     */
    boolean hasPermission(CommandSender sender);
}
