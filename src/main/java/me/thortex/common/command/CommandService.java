package me.thortex.common.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import me.thortex.common.service.Service;
import me.thortex.common.utility.Colors;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages command calling through {@link PlayerCommandPreprocessEvent}.
 * <p>
 * It also handles command storage.
 *
 * @author Thortex
 */
@Singleton
@Data
public class CommandService implements Service, Listener {
    private final JavaPlugin plugin;
    private final Map<List<String>, Command> commands;

    @Inject
    public CommandService(JavaPlugin plugin) {
        this.plugin = plugin;
        commands = new HashMap<>();
    }

    @Override
    public void start() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void stop() {
        HandlerList.unregisterAll(this);
    }

    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();

        String label;
        String[] args;

        // If there's more than 1 argument, separate the label and retrieve the arguments
        if (message.contains(" ")) {
            label = message.split(" ")[0].replaceAll("/", "");
            args = message.substring(message.indexOf(' ') + 1).split(" ");
        } else {
            label = message.replaceAll("/", "");
            args = new String[]{};
        }

        Command command = getCommandFromLabel(label);
        if (command != null) {
            event.setCancelled(true);

            if (command.hasPermission(player)) {
                command.execute(player, args);
            } else {
                player.sendMessage(Colors.RED + "You are not allowed to do that!");
            }
        }
    }

    /**
     * Find the command that uses the given label.
     *
     * @param label the label
     * @return the command if it exists
     */
    public Command getCommandFromLabel(String label) {
        for (List<String> aliases : commands.keySet()) {
            if (aliases.contains(label)) {
                return commands.get(aliases);
            }
        }
        return null;
    }
}
