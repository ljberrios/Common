package me.thortex.common.command;

import com.google.inject.Injector;
import com.google.inject.Singleton;
import me.thortex.common.AbstractLoader;

import java.util.List;
import java.util.Map;

/**
 * Handles all command loading/unloading functionality.
 *
 * @author Thortex
 */
@Singleton
public class CommandLoader extends AbstractLoader<Command> {
    private final Injector injector;

    public CommandLoader(Injector injector, String packageName) {
        super(injector, packageName, Command.class);
        this.injector = injector;
    }

    @Override
    public void startAll() {
        getInstances().forEach(cmd -> getCommands().put(cmd.getAliases(), cmd));
    }

    @Override
    public void stopAll() {
        getInstances().forEach(cmd -> getCommands().remove(cmd.getAliases()));
    }

    /**
     * Gets all the stored commands in {@link CommandService}.
     *
     * @return the commands
     */
    public Map<List<String>, Command> getCommands() {
        return injector.getInstance(CommandService.class).getCommands();
    }
}
