package me.thortex.common.task;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import me.thortex.common.Prioritized;
import me.thortex.common.service.Service;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Handles the main {@link ExecutorService}.
 *
 * @author Thortex
 */
@Singleton
@Data
@Prioritized
public class TaskService implements Service {
    private final JavaPlugin plugin;
    private final ExecutorService executor;

    @Inject
    public TaskService(JavaPlugin plugin) {
        this.plugin = plugin;
        executor = Executors.newCachedThreadPool();
    }

    @Override
    public void start() {
        plugin.getLogger().info("Started executor");
    }

    @Override
    public void stop() {
        plugin.getLogger().info("Shutting down executor and waiting for any pending tasks...");
        executor.shutdown();
        try {
            executor.awaitTermination(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}