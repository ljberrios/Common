package me.thortex.common.menu;

import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a GUI in the game.
 *
 * @author Thortex
 */
@Data
public abstract class Menu {
    private final Player holder;

    private final Map<Integer, ItemStack> items;

    private Inventory inventory;
    private boolean modifiable;

    public Menu(Player holder, String title, Object size, boolean modifiable) {
        this.holder = holder;
        this.modifiable = modifiable;

        items = new HashMap<>();

        if (size instanceof InventoryType) {
            inventory = Bukkit.getServer().createInventory(holder, (InventoryType) size, title);
        } else if (size instanceof Integer) {
            inventory = Bukkit.getServer().createInventory(holder, (int) size, title);
        } else {
            throw new IllegalArgumentException("Size must be an InventoryType or an Integer");
        }
    }

    public void open(MenuService service, boolean itemFlags) {
        if (service.getMenu(holder) == null) {
            addItems();
            build(service, itemFlags);
        }
        holder.openInventory(inventory);
    }

    public abstract void onClick(InventoryClickEvent event);

    protected abstract void addItems();

    protected void addItem(int slot, ItemStack item) {
        items.put(slot, item);
    }

    protected void build(MenuService service, boolean itemFlags) {
        try {
            inventory.clear();
            items.entrySet().forEach(entry -> {
                ItemStack item = entry.getValue();
                if (itemFlags) {
                    ItemMeta meta = item.getItemMeta();
                    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                    meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
                    meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
                    meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
                    item.setItemMeta(meta);
                }
                inventory.setItem(entry.getKey(), item);
            });

            service.addMenu(this);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
