package me.thortex.common.menu;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.Data;
import me.thortex.common.service.Service;
import me.thortex.common.update.UpdateEvent;
import me.thortex.common.update.UpdateType;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Handles all menus.
 *
 * @author Thortex
 */
@Singleton
@Data
public class MenuService implements Service, Listener {
    private final JavaPlugin plugin;
    private final Set<Menu> menus;

    @Inject
    public MenuService(JavaPlugin plugin) {
        this.plugin = plugin;
        menus = new HashSet<>();
    }

    @Override
    public void start() {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public void stop() {
        HandlerList.unregisterAll(this);
    }

    /**
     * Add a menu.
     *
     * @param menu the menu
     */
    public void addMenu(Menu menu) {
        menus.add(menu);
    }

    /**
     * Get a menu from the holder.
     *
     * @param holder the holder
     * @return the menu
     */
    public Menu getMenu(Player holder) {
        for (Menu menu : menus) {
            if (holder.equals(menu.getHolder())) {
                return menu;
            }
        }
        return null;
    }

    @EventHandler
    public void onUpdate(UpdateEvent event) {
        if (event.getType() != UpdateType.TICK) return;
        for (Iterator<Menu> itr = menus.iterator(); itr.hasNext(); ) {
            Inventory inventory = itr.next().getInventory();
            List<HumanEntity> viewers = inventory.getViewers();
            if (viewers == null || viewers.isEmpty()) {
                itr.remove();
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        ItemStack item = event.getCurrentItem();
        Inventory inventory = event.getClickedInventory();
        if (inventory == null || event.getSlotType() == InventoryType.SlotType.OUTSIDE ||
            (item == null || item.getType() == Material.AIR) || inventory.getTitle() == null) {
            return;
        }

        callInventoryEvent(inventory, event);
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent event) {
        Inventory inventory = event.getInventory();
        if (inventory == null || inventory.getTitle() == null || event.getCursor() == null) {
            return;
        }

        callInventoryEvent(inventory, event);
    }

    private <T extends Cancellable> void callInventoryEvent(Inventory inventory, T event) {
        menus.stream().filter(menu -> inventory.getTitle().equalsIgnoreCase(
            menu.getInventory().getTitle())).forEach(menu -> {
            if (!menu.isModifiable()) {
                event.setCancelled(true);
            }

            if (event instanceof InventoryClickEvent) {
                menu.onClick((InventoryClickEvent) event);
            }
        });
    }
}
