package me.thortex.common;

import com.google.inject.Injector;
import me.thortex.common.utility.ClassFinder;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Abstract class for loading/unloading of services and commands.
 *
 * @author Thortex
 */
public abstract class AbstractLoader<T> {
    private final Injector injector;
    private final String packageName;
    private final Class<T> clazz;

    public AbstractLoader(Injector injector, String packageName, Class<T> clazz) {
        this.injector = injector;
        this.packageName = packageName;
        this.clazz = clazz;
    }

    /**
     * Get all the instances of the classes to load.
     *
     * @return the list of instances
     */
    public List<T> getInstances() {
        return ClassFinder.getFixedSubtypesOf(packageName, clazz).stream()
            .map(injector::getInstance)
            .collect(Collectors.toList());
    }

    /**
     * Load all the classes.
     * <p>
     * In the case of services, we use a start method for them.
     * When start is called, all the services are started with their
     * start method.
     */
    public abstract void startAll();

    /**
     * Un-load all the classes.
     * <p>
     * In the case of services, we use a stop method for them.
     * When stop is called, all the services are stopped with their
     * stop method.
     */
    public abstract void stopAll();
}
